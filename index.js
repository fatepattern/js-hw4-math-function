let numberOne;
let numberTwo;
let operation;

while(numberOne === null || isNaN(numberOne) || numberOne === ''){
    numberOne = prompt("Enter your first number");
}

while(numberTwo === null || isNaN(numberTwo) || numberTwo === ''){
    numberTwo = prompt("What is your second number");
}

operation = prompt("What is your operation");

let mathFunction = (numberOne, numberTwo, operation) => {

    switch(operation){
        case '/': return numberOne/numberTwo;
        case '*': return numberOne * numberTwo;
        case '+': return Number(numberOne) + Number(numberTwo);
        case '-': return numberOne - numberTwo;
        default: return "Not an operation"
    }
}

alert(mathFunction(numberOne, numberTwo, operation));